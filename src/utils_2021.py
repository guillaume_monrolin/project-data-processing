# -*- coding: utf-8 -*-

import numpy as np
# from enum import Enum
import time
import os
from skimage.transform import resize
from keras.preprocessing.image import load_img, img_to_array

record_time = np.zeros((10, 1))


def start_time(index):
    record_time[index] = time.time()


def stop_time(index):
    record_time[index] = time.time() - record_time[index]
    return str(float(record_time[index]))[0:7]


def set_labels(data, col2, col8):
    data[data[:, col2] == 0, col2] = 'ARTIFICIEL'
    data[data[:, col2] == 1, col2] = 'NATUREL'
    data[data[:, col8] == 2, col8] = 'COTE'
    data[data[:, col8] == 3, col8] = 'FORET'
    data[data[:, col8] == 4, col8] = 'AUTOROUTE'
    data[data[:, col8] == 5, col8] = 'VILLE'
    data[data[:, col8] == 6, col8] = 'MONTAGNE'
    data[data[:, col8] == 7, col8] = 'OPEN_COUNTRY'
    data[data[:, col8] == 8, col8] = 'RUE'
    data[data[:, col8] == 9, col8] = 'GRANDBATIMENT'
    return data


def unset_labels(data, col2, col8):
    data[data[:, col2] == 'ARTIFICIEL', col2] = 0
    data[data[:, col2] == 'NATUREL', col2] = 1
    data[data[:, col8] == 'COTE', col8] = 2
    data[data[:, col8] == 'FORET', col8] = 3
    data[data[:, col8] == 'AUTOROUTE', col8] = 4
    data[data[:, col8] == 'VILLE', col8] = 5
    data[data[:, col8] == 'MONTAGNE', col8] = 6
    data[data[:, col8] == 'OPEN_COUNTRY', col8] = 7
    data[data[:, col8] == 'RUE', col8] = 8
    data[data[:, col8] == 'GRANDBATIMENT', col8] = 9
    return data


def show_database(data, col2, col8):
    print("Contenu Total: " + str(len(data)))
    print("Classée Artificielle: " + str(len(data[data[:, col2] == 'ARTIFICIEL'])))
    print("Classée Naturelle: " + str(len(data[data[:, col2] == 'NATUREL'])))
    print("Classée Côte: " + str(len(data[data[:, col8] == 'COTE'])))
    print("Classée Forêt: " + str(len(data[data[:, col8] == 'FORET'])))
    print("Classée Autoroute: " + str(len(data[data[:, col8] == 'AUTOROUTE'])))
    print("Classée Ville: " + str(len(data[data[:, col8] == 'VILLE'])))
    print("Classée Montagne: " + str(len(data[data[:, col8] == 'MONTAGNE'])))
    print("Classée Paysage Ouvert: " + str(len(data[data[:, col8] == 'OPEN_COUNTRY'])))
    print("Classée Rue: " + str(len(data[data[:, col8] == 'RUE'])))
    print("Classée Grand Batiment: " + str(len(data[data[:, col8] == 'GRANDBATIMENT'])))
    print(" ")
    X = [len(data[data[:, col2] == 'ARTIFICIEL']), len(data[data[:, col2] == 'NATUREL'])]
    Y = [len(data[data[:, col8] == 'COTE']), len(data[data[:, col8] == 'FORET']),
         len(data[data[:, col8] == 'AUTOROUTE']), len(data[data[:, col8] == 'VILLE']),
         len(data[data[:, col8] == 'MONTAGNE']), len(data[data[:, col8] == 'OPEN_COUNTRY']),
         len(data[data[:, col8] == 'RUE']), len(data[data[:, col8] == 'GRANDBATIMENT'])]
    return X, Y
