# -*- coding: utf-8 -*-
"""
    Main file
    @author: Guillaume Monrolin
"""
# Standard library imports
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split

# Local library
import utils_2021 as utils
from Partie1_2021 import PCA_Utils
from aRemplir_Partie2 import Classifier, ClassifierKind

if __name__ == "__main__":

    plt.close('all')

    # Main
    # utils.shake_database('./database.csv')  #si on veut changer l'ordre dans la database
    # Partie 1.1 Recuperation de la base de données type DataFrame

    database = pd.read_csv('./database.csv')
    data = np.array(database)
    utils.show_database(data, 1, 2)

    # Paramètres
    # Nombre d'images dans la base d'apprentissage 70% de la base de données
    RangeApprentissage = 1882

    # Etude sur catégorie 2 classes == 0 ou 8 classes == 1
    ColLabels = 1

    # utils.show_database(database)

    # Partie 1
    pca_util = PCA_Utils(database)
    pca_util.bar_chart()
    pca_util.stats()
    pca = pca_util.get_PCA(n_components=3)
    pca_util.plot_PCA(pca)

    #Partie 2
    data_train, data_test, label_train, label_test = train_test_split(data[:, 3:], data[:, 1:3], random_state=1)

    dtc = Classifier(ClassifierKind.DECISION_TREE)
    dtc.predict_and_plot(gui_id="Classes",
                         data_train=data_train, data_test=data_test,
                         label_train=label_train[:, 0], label_test=label_test[:, 0])
    dtc = Classifier(ClassifierKind.DECISION_TREE)
    dtc.predict_and_plot(gui_id="Subclasses",
                         data_train=data_train, data_test=data_test,
                         label_train=label_train[:, 1], label_test=label_test[:, 1])

    mlp = Classifier(ClassifierKind.MLP)
    mlp.predict_and_plot(gui_id="Classes - 10 neurons",
                         data_train=data_train, data_test=data_test,
                         label_train=label_train[:, 0], label_test=label_test[:, 0])
    mlp = Classifier(ClassifierKind.MLP, neurons=75)
    mlp.predict_and_plot(gui_id="Subclasses - 10 neurons",
                         data_train=data_train, data_test=data_test,
                         label_train=label_train[:, 1], label_test=label_test[:, 1])

    mlp = Classifier(ClassifierKind.MLP, neurons=75)
    mlp.predict_and_plot(gui_id="Classes - 75 neurons",
                         data_train=data_train, data_test=data_test,
                         label_train=label_train[:, 0], label_test=label_test[:, 0])
    mlp = Classifier(ClassifierKind.MLP, neurons=75)
    mlp.predict_and_plot(gui_id="Subclasses - 75 neurons", data_train=data_train, data_test=data_test,
                         label_train=label_train[:, 1], label_test=label_test[:, 1])

    mlp = Classifier(ClassifierKind.MLP, neurons=200)
    mlp.predict_and_plot(gui_id="Classes - 200 neurons",
                         data_train=data_train, data_test=data_test,
                         label_train=label_train[:, 0], label_test=label_test[:, 0])
    mlp = Classifier(ClassifierKind.MLP, neurons=200)
    mlp.predict_and_plot(gui_id="Subclasses - 200 neurons",
                        data_train=data_train, data_test=data_test,
                        label_train=label_train[:, 1], label_test=label_test[:, 1])
