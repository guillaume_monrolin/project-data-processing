# -*- coding: utf-8 -*-
"""
    Partie 2
    @author: Guillaume Monrolin
"""
# Standard library imports
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
from sklearn import tree
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import cross_val_score
from enum import Enum
# Local library
import utils_2021


# PARTIE 2
def matrice_confusion(y_true, y_pred):
    """ Définition : Calcul la matrice de confusion 
        Entrée : Tableau expert, Tableau préditpour
        Sortie : Matrice de confusion, Performance moyenne
    """
    return confusion_matrix(y_true, y_pred)


class ClassifierKind(Enum):
    DECISION_TREE = "Decision tree"
    MLP = "MLP"


class Classifier(object):

    def __init__(self, kind, neurons=10):

        self.kind = kind

        if kind == ClassifierKind.DECISION_TREE:
            self.clf = tree.DecisionTreeClassifier()
        elif kind == ClassifierKind.MLP:
            self.clf = MLPClassifier(hidden_layer_sizes=neurons)

    def predict_and_plot(self, data_train, data_test, label_train, label_test, gui_id=""):
        # fit data
        self.clf = self.clf.fit(data_train, label_train)
        # predict
        label_pred = self.clf.predict(data_test)
        # accuracy and cross validation
        print(self.kind.value + " " + gui_id + " Accuracy:", metrics.accuracy_score(label_test, label_pred))
        print(self.kind.value + " " + gui_id + " Cross validation:", cross_val_score(self.clf, data_train, label_train))
        # confusion matrix
        plot_confusion_matrix(self.clf, data_test, label_test)
        plt.title("Confusion matrix " + self.kind.value + " " + gui_id)
        plt.xticks(rotation="vertical")
        plt.show(block=False)