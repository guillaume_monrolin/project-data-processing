# -*- coding: utf-8 -*-
"""
    Partie 1
    @author: Guillaume Monrolin
"""
# Standard library imports
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import matplotlib.patches as mpatches

# Local library
import utils_2021 as utils


class PCA_Utils(object):
    def __init__(self, data):
        self.initial_data = data
        data = np.array(data)
        self.data = data[:, 3:]
        self.classes = data[:, 1]
        self.subclasses = data[:, 2]
        self.n = self.data.shape[0]
        self.p = self.data.shape[1]

    def stats(self):
        print(f"Moyennes :{self.data.mean(axis=0)}")
        print(f"Variance :{self.data.var(axis=0)}")

    def normalize(self):
        sc = StandardScaler()
        Z = sc.fit_transform(self.data)
        return np.array(Z)

    def get_class_l(self):
        return list(set(self.classes))

    def get_subclass_l(self):
        return list(set(self.subclasses))

    def get_data_l(self):
        return list(set(self.data))

    def get_PCA(self, n_components=3):
        pca = PCA(svd_solver="full", n_components=n_components)
        return pca.fit_transform(self.data)

    def bar_chart(self):
        np.count_nonzero(self.classes == "NATUREL")
        n_classes = [np.count_nonzero(self.classes == "NATUREL"), np.count_nonzero(self.classes == "ARTIFICIEL")]
        n_subclasses = [np.count_nonzero(self.subclasses == "COTE"),
                        np.count_nonzero(self.subclasses == "FORET"),
                        np.count_nonzero(self.subclasses == "AUTOROUTE"),
                        np.count_nonzero(self.subclasses == "VILLE"),
                        np.count_nonzero(self.subclasses == "MONTAGNE"),
                        np.count_nonzero(self.subclasses == "OPEN_COUNTRY"),
                        np.count_nonzero(self.subclasses == "RUE"),
                        np.count_nonzero(self.subclasses == "GRANDBATIMENT")]
        plt.figure()
        plt.barh(y=["NATUREL", "ARTIFICIEL"], width=n_classes)
        plt.title("Repartition des classes en nombre")
        plt.show()

        plt.figure()
        plt.barh(y=["COTE", "FORET", "AUTOROUTE", "VILLE", "MONTAGNE",
                   "OPEN_COUNTRY", "RUE", "GRANDBATIMENT"],
                width=n_subclasses)
        plt.title("Repartition des sous classes en nombre")
        plt.show()

    def plot_PCA(self, pca):
        colors = ['deeppink', 'purple', 'blue', 'chartreuse', 'orange', 'cyan', 'orangered', 'black']

        plt.figure()
        for color, subclass_name in zip(colors, self.get_subclass_l()):
            plt.scatter(pca[self.subclasses == subclass_name, 0],
                        pca[self.subclasses == subclass_name, 1],
                        color=color, alpha=.8, lw=2, label=subclass_name)
        plt.legend(loc='best', shadow=False, scatterpoints=1)
        plt.title('Subclasse PCA')
        plt.show()

        plt.figure()
        for color, class_name in zip(colors, self.get_class_l()):
            plt.scatter(pca[self.classes == class_name, 0],
                        pca[self.classes == class_name, 1],
                        color=color, alpha=.8, lw=2, label=class_name)
        plt.legend(loc='best', shadow=False, scatterpoints=1)
        plt.title('Classes PCA')

        plt.show()
