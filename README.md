# Projet de Traitement de données

Dans ce repository vous trouverez les sources du projet réalisé dans 
le cadre de mes études en école d'ingénieur. Dans ce projet nous nous 
proposons d'étudier les méthodes d'apprentissages algorithmiques sur un jeu 
d'images.

Les images sont classifiées selon deux catégories : sa classe et sa sous-classe.
La classe de l'image peut est être : 
- Naturelles (images de nature)
- Artificielles (constructions humaines)

La sous-classe de l'image est une de ces 8 catégories :  
- Côte (plage, mer…)  
- Forêt  
- Autoroute  
- Ville (Photo prise de loin de l’ensemble de la ville)  
- Montagne  
- Paysage ouvert (landes, désert…)  
- Rue  
- Grand bâtiment 

Le but du projet est d'analyser le jeu de données puis de mettre en place des 
plusieurs algorithmes de classification afin de comparer leurs
performances.

## Partie I : Analyse des données
Puisque chaque image possède 27 vecteurs de caractérisation, il est difficile
d'analyser les données et d'en comprendre le sens. Ainsi nous allons utiliser 
la méthode d'analyse en composantes principales afin d'étudier la corrélation
des différentes composantes d'une image pour ensuite essayer de déduire une
règle d'association.
Au préalable il est intéressant de comprendre comment notre jeu de données est
construit : 

![image](./docs/chart/n_classes.png "Nombre d'images par classes") 
![image](./docs/chart/n_subclasses.png "Nombre d'images par sous-classes")

On voit donc que notre jeu de données est assez bien réparti. Au niveau de 
l'analyse en composantes principales, nous avons utilisé les packages python 
sklearn et matplotlib qui nous ont permis de tracer une répartition en nuage de
point de notre décomposition :

![image](./docs/chart/pca_classes.png "ACP en fonction des classes") 
![image](./docs/chart/pca_subclasses.png "ACP en fonction des sous classes")

On se rend compte que malgré l'ACP nos données sont très entremêlées et donc 
fortement corrélées. Ainsi, la classification des images ne sera pas facile
et précise.

## Partie 2 : Machine Learning - Classification automatique supervisée

Le but de cette partie est de mettre en place deux types d'algorithmes afin 
d'apprendre à classifier ce type de données : 
- L'algorithme a arbre de décision
- L'algorithme Multi Layer Perceptron

Pour les deux algorithmes nous utilisons la bibliothèque sklearn. Pour ce faire la
méthode est la même pour les deux algo : 
1) Créer une instance de classificateur (DecisionTree ou MLP)
   ```python
    if kind == ClassifierKind.DECISION_TREE:
        self.clf = tree.DecisionTreeClassifier()
    elif kind == ClassifierKind.MLP:
        self.clf = MLPClassifier(hidden_layer_sizes=neurons)
   ```
2) L'entrainer avec un jeu de données spécifique
   ```python
   self.clf = self.clf.fit(data_train, label_train)
   ```
3) Tester avec un jeu différent que celui d'entrainement. Il est important que
le jeu d'entrainement ne soit pas le même que celui de test sinon nous tomberons
dans le cas où l'algorithme est surentraîné et donc trop spécifique et ne 
pourras plus prédire correctement les prochains modèles.
   ```python
   label_pred = self.clf.predict(data_test)
   ```
4) Enfin, on étudie la précision puis on trace une matrice de confusion pour déterminer 
   sa performance. Cette matrice de confusion nous permettra d'évaluer 4 catégories
   de réponse de l'algorithme : 
    - Les True Positives (TP): cas où la prédiction est positive et la valeure réelle
    est positive
    - Les True Negatives (TN): prédiction négatives, valeur réelle négatives
    - Les False Positives (FP): prédiction positives, valeur réelle négative
    - Les False Négatives (FN): prédiction négatives, valeur réelle positive
    ```python
    # Accuracy and Cross Val
    print(self.kind.value + " " + gui_id + " Accuracy:", metrics.accuracy_score(label_test, label_pred))
    print(self.kind.value + " " + gui_id + " Cross validation:", cross_val_score(self.clf, data_train, label_train))
    # Confusion Matrix
    plot_confusion_matrix(self.clf, data_test, label_test)
    plt.title("Confusion matrix " + self.kind.value + " " + gui_id)
    plt.xticks(rotation="vertical")
    plt.show(block=False)
     ```
    
Ainsi dans notre analyse nous cherchons à maximiser notre précision par la maximisation
des True Positives et True Negatives. Voici une illustration d'une matrice de confusion
illustrant les concepts précédemment évoqués : 

![image](./docs/chart/th.jpeg "ACP en fonction des sous classes")

Étudions donc le taux de précision de nos modèles générés.

Méthode                | Type de données | Précision
---------------------- | --------------- | ----------
DecisionTree           | Classe          | 0,88
DecisionTree           | Sous-classe     | 0,55
MLP - 10 neurones      | Classe          | 0,909 
MLP - 10 neurones      | Sous-classe     | 0,738 
MLP - 75 neurones      | Classe          | 0,918 
MLP - 75 neurones      | Sous-classe     | 0,743 
MLP - 200 neurones     | Classe          | 0,92
MLP - 200 neurones     | Sous-classe     | 0,737 

Si on regarde les précisions on se rend compte que globalement lorsque l'on
utilise la méthode d'arbre de décision notre modèle est beaucoup moins précis
que par la méthode MLP. Cependant en utilisant la méthode MLP On se rend compte
qu'il faut être vigilant car augmenter le nombre de neurones utilisé ne permet 
pas tout le temps d'être plus précis. En effet on remarque que lorsque nous 
utilisons 200 neurones nous ne gagnons pas en précision sur les classes et en plus
nous perdons en précision sur les sous-classes.

Regardons maintenant les matrices de confusion afin d'étudier les 
performances de nos algorithmes :

Méthode               | Classes | Sous-classes
---------------------- | --------------- | ----------
DecisionTree           | ![image](./docs/chart/cm_tree_classes.png)          | ![image](./docs/chart/cm_tree_subclasses.png)
MLP - 10 neurones      | ![image](./docs/chart/cm_mlp_10_classes.png)          | ![image](./docs/chart/cm_mlp_10_subclasses.png) 
MLP - 75 neurones      | ![image](./docs/chart/cm_mlp_75_classes.png)          | ![image](./docs/chart/cm_mlp_75_subclasses.png)
MLP - 200 neurones     | ![image](./docs/chart/cm_mlp_200_classes.png)         | ![image](./docs/chart/cm_mlp_200_subclasses.png)

Les modèles obtenus sont intéressant car nous pouvons constater que les algorithmes
DecisionTree et MLP sont aussi performant pour détecter les TP et TN en ce qui concerne 
les classes d'images. Par contre on voit une grosse différence de performance pour les 
sous classes. 
Toutefois on remarque que l'algorithme MLP produit une très faible quantité de FN comparé
au DecisionTree.

## Conclusion
Nous avons donc pu comparer deux types d'algorithmes : DecisionTree et MLP ; dans 
deux types de données les classes et les sous-classes d'images. Nous avons pu nous
apercevoir que l'algorithme de MLP était plus précis sur les deux types de données 
que le DecisionTree. Cependant, plus l'algorithme MLP utilise de neurones plus la 
génération de modèles est longue, de plus si le nombre de neurones est trop élevé nous
observons une baisse de la précision. Si maintenant nous comparons la performance des
algorithmes, c'est-à-dire la justesse des prédictions les deux algorithmes sont 
plus où moins équivalent sur un faible nombre de classifications (pour les classes 
d'images dans notre cas) mais sur un grand nombre de classifications (les sous-classses)
l'algorithme MLP est beaucoup plus performant.
